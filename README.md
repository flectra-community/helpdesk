# Flectra Community / helpdesk

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[helpdesk_mgmt_rating](helpdesk_mgmt_rating/) | 2.0.1.1.0|         This module allows customer to rate the assistance received        on a ticket.        
[helpdesk_mgmt_timesheet](helpdesk_mgmt_timesheet/) | 2.0.1.0.1| Add HR Timesheet to the tickets for Helpdesk Management.
[helpdesk_type](helpdesk_type/) | 2.0.1.0.1| Add a type to your tickets
[helpdesk_mgmt](helpdesk_mgmt/) | 2.0.1.9.0|         Helpdesk
[helpdesk_mgmt_project](helpdesk_mgmt_project/) | 2.0.1.0.0| Add the option to select project in the tickets.
[helpdesk_motive](helpdesk_motive/) | 2.0.1.0.1| Keep the motive 


